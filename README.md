![Eudaphobase icon](https://www.eudaphobase.eu/wp-content/uploads/eudaphobase-logotype.svg)
# EUdaphobase data fields ontology
[![pipeline status](https://gitlab.com/jfaldanam/edfo/badges/master/pipeline.svg)](https://gitlab.com/jfaldanam/edfo/-/commits/master)
[![Latest Release](https://gitlab.com/jfaldanam/edfo/-/badges/release.svg)](https://gitlab.com/jfaldanam/edfo/-/releases) 


This repository includes the [base ontology](edfo.base.owl), as well as a [python script](generate_ontology.py) to populate the ontology from [the Edaphobase data fields data](https://edaphobase.org/edaphobase-datafields/data.js).

The data fields ontology is automatically re-generated the last day of each month, and is available at `https://w3id.org/edfo`, as RDF/XML or the html documentation, depending to content negotiation.


# How to generate the ontology instances
To generate the scripts `Python>=3` is required.

1. Install the required dependencies

`pip install -r requirements.txt`

2. Execute the script [generate_ontology.py](generate_ontology.py)

```console
$ python generate_ontology.py
```

# Documentation
Use [`pylode`](https://github.com/RDFLib/pyLODE) to generate the documentation for the main ontology or the generated ones from the EDaphobase API:
```console
$ python -m pylode edfo.owl -o docs.html
```

# Changelogs
> **Warning**: The updates to the actual data fields data (collected from EDaphobase directly) is not controlled in this repository, there may be changes on the resulting ontology without being listed here.

## Changelog v1.0.0
First major release
