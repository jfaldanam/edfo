import json
import re
from functools import cache
from typing import Any

import requests
from rdflib import Graph, Literal, Namespace
from rdflib.namespace import  RDFS, OWL


edfo = Namespace("https://w3id.org/edfo#")

# Should be done, only clean up and fix missing parents
def hierarchical_categorical_types(lists_data: dict, graph: Graph)->dict[str, Any]:
    
    # Add top level class TODO: Move to base ontology
    graph.add(
        (
            edfo["CategoricalType"],
            RDFS.subClassOf,
            OWL.Thing,
        )
    )


    data = {}
    for key, values in lists_data.items():

        data_for_key = {}


        # We will use the english label as the key for ordering
        parent_per_level = {
            0: data_for_key
        }
        for elem in values:
            current_elem = {
                "name_en": elem['en'],
                "name_de": elem['de'],
            }

            level_of_parent = elem["level"]-1
            parent = parent_per_level.get(level_of_parent, data_for_key)

            if level_of_parent == 0:
                parent[elem['en']] = current_elem

                # Define uri and add to ontology
                edfo_uri = edfo[str(hash(elem['en']))]
                graph.add(
                    (
                        edfo_uri,
                        RDFS.subClassOf,
                        edfo.CategoricalType,
                    )
                )

                graph.add(
                    (
                        edfo_uri,
                        RDFS.label,
                        Literal(elem['en'], lang="en"),
                    )
                )

                graph.add(
                    (
                        edfo_uri,
                        RDFS.label,
                        Literal(elem['de'], lang="de"),
                    )
                )
            else:
                if "children" not in parent:
                    parent["children"] = {}

                # TODO this should not happen, there must always be a parent
                if "name_en" not in parent:
                    print("Warning: parent missing")
                    parent["name_en"] = "WARNING = MISSING"
                
                parent["children"][elem['en']] = current_elem

                edfo_uri = edfo[str(hash(elem['en']))]

                graph.add(
                    (
                        edfo_uri,
                        RDFS.subClassOf,
                        edfo[str(hash(parent["name_en"]))],
                    )
                )

                graph.add(
                    (
                        edfo_uri,
                        RDFS.label,
                        Literal(elem['en'], lang="en"),
                    )
                )

                graph.add(
                    (
                        edfo_uri,
                        RDFS.label,
                        Literal(elem['de'], lang="de"),
                    )
                )

            parent_per_level[elem["level"]] = current_elem

        data[key] = data_for_key

    return data


def process_metadata(metadata: dict, hierarchical_categories: dict, graph: Graph):

    graph.add(
        (
            edfo["MetadataField"],
            RDFS.subClassOf,
            OWL.Thing,
        )
    )


    for doc in metadata:
        process_recursive_metadata(graph, hierarchical_categories, doc, parent_uri=edfo["MetadataField"])

    return metadata

def process_recursive_metadata(graph, hierarchical_categories, document: dict, parent_uri):

    uri = edfo[str(hash(document["name_en"]))]

    graph.add(
        (
            uri,
            RDFS.subClassOf,
            parent_uri,
        )
    )

    for language in ["en", "de"]:
        graph.add(
            (
                uri,
                RDFS.label,
                Literal(document[f'name_{language}'], lang=language),
            )
        )

        if f'description_{language}' in document:
            graph.add(
                (
                    uri,
                    edfo["description"],
                    Literal(document[f'description_{language}'], lang=language),
                )
            )
        if f'subtitle_{language}' in document:
            graph.add(
                (
                    uri,
                    edfo["subtitle"],
                    Literal(document[f'subtitle_{language}'], lang=language),
                )
            )

    # TODO missing extra properties such as cardinality, mandatory, input, etc

    if "children" in document:
        for child in document["children"]:
            process_recursive_metadata(graph, hierarchical_categories, child, parent_uri=uri)

    if "type" in document:
        if document["type"] == "list":
            if "list" in document:
                assert document["list"] in hierarchical_categories, f"List {document['list']} not found: {document}"
        
    return None

def process_datafields(data_fields: dict, hierarchical_categories: dict, graph: Graph) -> dict[str, Any]:
    graph.add(
        (
            edfo["DataField"],
            RDFS.subClassOf,
            OWL.Thing,
        )
    )

    for doc in data_fields:
        process_recursive_datafields(graph, hierarchical_categories, doc, parent_uri=edfo["DataField"])

    return data_fields

def process_recursive_datafields(graph, hierarchical_categories, document: dict, parent_uri):

    uri = edfo[str(hash(document["name_en"]))]

    graph.add(
        (
            uri,
            RDFS.subClassOf,
            parent_uri,
        )
    )

    for language in ["en", "de"]:
        graph.add(
            (
                uri,
                RDFS.label,
                Literal(document[f'name_{language}'], lang=language),
            )
        )

        if f'description_{language}' in document:
            graph.add(
                (
                    uri,
                    edfo["description"],
                    Literal(document[f'description_{language}'], lang=language),
                )
            )


    # TODO missing extra properties such as cardinality, mandatory, input, etc

    if "children" in document:
        for child in document["children"]:
            process_recursive_datafields(graph, hierarchical_categories, child, parent_uri=uri)

    if "type" in document:
        if document["type"] == "List element":
            if "list" in document:
                assert document["list"]["name"] in hierarchical_categories, f"List {document['list']} not found: {document}"
        
    return None

@cache
def retrieve_data_fields():
    """
    Retrieve the data fields from Edaphobase and parse its output
    """
    response = requests.get('https://edaphobase.org/edaphobase-datafields/data.js')
    response.raise_for_status()

    text = response.text
    metadata = text.split('var dataFields = ')[0].removeprefix("var metadataFields = ").strip().removesuffix(";")
    
    data_fields = text.split('var dataFields = ')[1].split('var lists = ')[0].strip().removesuffix(";")

    # Remove trailing commas on dicts
    data_fields = re.sub(r',(?=\s*})', '', data_fields)
    # Remove Java(script) style comments, taking care of not confusing // with URLs
    # For single line comments check that the preceding element is not :
    data_fields = re.sub(r'/\*.*?\*/', '', data_fields, flags=re.DOTALL)
    data_fields = re.sub(r'(?<!:)//.*', '', data_fields)

    lists = text.split('var lists = ')[1].strip().removesuffix(";")
    # Remove trailing commas on lists
    lists = re.sub(r',(?=\s*])', '', lists)
    # Remove Java(script) style comments
    lists = re.sub(r'/\*.*?\*/', '', lists, flags=re.DOTALL)

    return {"metadata": json.loads(metadata), "data_fields": json.loads(data_fields), "lists": json.loads(lists)}



if __name__ == "__main__":
    data_fields_data =  retrieve_data_fields()

    graph = Graph()

    graph.bind("edfo", edfo)

    graph.parse("edfo-base.owl", format="xml")

    hierarchical_categories = hierarchical_categorical_types(data_fields_data["lists"], graph)

    process_metadata(data_fields_data["metadata"], hierarchical_categories, graph)

    process_datafields(data_fields_data["data_fields"], hierarchical_categories, graph)

    graph.serialize("edfo.owl", format="xml")